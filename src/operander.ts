export type OperanderStatement    = [any, string, any];
export type OperanderChainCmd     = 'AND' | 'OR';
export type OperanderStatementSet = [OperanderStatement, OperanderChainCmd];
export type OperanderResolver     = any | ((key: string) => boolean);
export interface OperanderOperatorMap {[key: string]: (v1: any, v2: any) => boolean};

export class Operander {
  static OPERATORS: OperanderOperatorMap = {
    '<':        (v1: number, v2: number) => v1 < v2,
    '<=':       (v1: number, v2: number) => v1 <= v2,
    '>':        (v1: number, v2: number) => v1 > v2,
    '>=':       (v1: number, v2: number) => v1 >= v2,
    'between':  (v: number, r: number[]) => r[0] < v && v < r[1],
    'in-range': (v: number, r: number[]) => r[0] <= v && v <= r[1],
    '==':       (v1: any, v2: any)       => v1 == v2,
    '===':      (v1: any, v2: any)       => v1 === v2,
    '!=':       (v1: any, v2: any)       => v1 != v2,
    '!==':      (v1: any, v2: any)       => v1 !== v2,
    'in':       (v: any, a: any)         => a.includes(v),
    '!in':      (v: any, a: any)         => !a.includes(v),
    'has':      (a: any, v: any)         => a.includes(v),
    '!has':     (a: any, v: any)         => !a.includes(v)
  };

  static ExtractVariables(rules: OperanderStatementSet | OperanderStatement, obj?: any): any {
    const variables: any = {};

    rules.forEach((r: OperanderStatementSet | OperanderStatement) => {
      if (Array.isArray(r)) {
        Object.assign(variables, this.ExtractVariables(r, obj));
      } else if (typeof r === 'string' && (r as string).startsWith('$.')) {
        variables[(r as string).substr(2)] = obj ? this.ResolveVariable(r, obj) : null;
      }
    });

    return variables;
  }

  static Process(rules : OperanderStatementSet | OperanderStatement, obj: OperanderResolver): boolean {
    let result;

    if (rules.length === 3 && Object.keys(Operander.OPERATORS).includes(rules[1])) {
      return this.Eval(rules, obj);
    }

    for(let i = 0; i < rules.length; i++ ) {
      if (Array.isArray(rules[i])) {
        result = this.Process(rules[i], obj);
        
      } else if (typeof rules[i] === 'string') {
        const upper = rules[i].toUpperCase();

        if (result) {
          if (upper === 'OR') {
            return true; // Prior result is true, anything after OR doesn't matter
          }
        } else if (upper === 'AND') {
          return false; // Prior result is false, anything after AND doesn't matter
        }
      }
    };

    return !!result;
  }

  static Eval(rule: OperanderStatement, obj: OperanderResolver) {
    const left  = this.ResolveVariable(rule[0], obj);
    const right = this.ResolveVariable(rule[2], obj);

    return this.OPERATORS[rule[1]](left, right);
  }

  static ResolveVariable(key: any, obj: OperanderResolver): any {
    if (typeof key === 'string' && key.startsWith('$.')) {
      return (typeof obj === 'function') 
        ? obj(key.substr(2))                // Call given resolver with key
        : this.SafeGet(obj, key.substr(2)); // Get value from given object
    }

    return key;
  }

  static SafeGet(obj: any, props: string): any {
    return obj && props.split('.').reduce(
      (result, prop) => result == null ? undefined : result[prop],
      obj
    );
  }
}
