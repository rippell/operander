var Operander = require("../dist/operander").Operander;

describe("Existence", () => {
    it("should exist", () => {
        expect(Operander).toBeTruthy();
    });

    it("should have known OPERATORS", () => {
        var OPERATORS = [
            '<',       '<=',
            '>',       '>=',
            'between', 'in-range',
            '==',      '===',
            '!=',      '!==',
            'in',      '!in',
            'has',     '!has'
        ];

        OPERATORS.forEach(f => expect(typeof Operander.OPERATORS[f]).toEqual('function'));
    });

    it("should have known functions", () => {
        var funcs = ['ExtractVariables', 'Process', 'Eval', 'ResolveVariable', 'SafeGet'];

        funcs.forEach(f => expect(typeof Operander[f]).toEqual('function'));
    });
});

describe("Operators", () => {
    it("<", () => {
        expect(Operander.OPERATORS['<'](1, 10)).toEqual(1 < 10);
        expect(Operander.OPERATORS['<'](10, 0)).toEqual(10 < 0);
    });
    it("<=", () => {
        expect(Operander.OPERATORS['<='](1, 10)).toEqual(1 <= 10);
        expect(Operander.OPERATORS['<='](5, 5)).toEqual(5 <= 5);
        expect(Operander.OPERATORS['<='](10, 1)).toEqual(10 <= 1);
    });

    it(">", () => {
        expect(Operander.OPERATORS['>'](1, 10)).toEqual(1 > 10);
        expect(Operander.OPERATORS['>'](10, 0)).toEqual(10 > 0);
    });
    it(">=", () => {
        expect(Operander.OPERATORS['>='](1, 10)).toEqual(1 >= 10);
        expect(Operander.OPERATORS['>='](5, 5)).toEqual(5 >= 5);
        expect(Operander.OPERATORS['>='](10, 1)).toEqual(10 >= 1);
    });

    it("between", () => {
        expect(Operander.OPERATORS['between'](0, [1, 10])).toEqual((1 < 0 && 0 < 10));
        expect(Operander.OPERATORS['between'](1, [1, 10])).toEqual((1 < 1 && 1 < 10));
        expect(Operander.OPERATORS['between'](5, [1, 10])).toEqual((1 < 5 && 5 < 10));
        expect(Operander.OPERATORS['between'](10, [1, 10])).toEqual((1 < 10 && 10 < 10));
        expect(Operander.OPERATORS['between'](11, [1, 10])).toEqual((1 < 11 && 11 < 10));
    });
    it("in-range", () => {
        expect(Operander.OPERATORS['in-range'](0, [1, 10])).toEqual((1 <= 0 && 0 <= 10));
        expect(Operander.OPERATORS['in-range'](1, [1, 10])).toEqual((1 <= 1 && 1 <= 10));
        expect(Operander.OPERATORS['in-range'](5, [1, 10])).toEqual((1 <= 5 && 5 <= 10));
        expect(Operander.OPERATORS['in-range'](10, [1, 10])).toEqual((1 <= 10 && 10 <= 10));
        expect(Operander.OPERATORS['in-range'](11, [1, 10])).toEqual((1 <= 11 && 11 <= 10));
    });

    it("==", () => {
        expect(Operander.OPERATORS['=='](true, true)).toEqual(true == true);
        expect(Operander.OPERATORS['=='](true, false)).toEqual(true == false);

        expect(Operander.OPERATORS['=='](true, 1)).toEqual(true == 1);
        expect(Operander.OPERATORS['=='](1, '1')).toEqual(1 == '1');
    });
    it("===", () => {
        expect(Operander.OPERATORS['==='](true, true)).toEqual(true === true);
        expect(Operander.OPERATORS['==='](true, false)).toEqual(true === false);

        expect(Operander.OPERATORS['==='](true, 1)).toEqual(true === 1);
        expect(Operander.OPERATORS['==='](1, '1')).toEqual(1 === '1');
    });

    it("!=", () => {
        expect(Operander.OPERATORS['!='](true, true)).toEqual(true != true);
        expect(Operander.OPERATORS['!='](true, false)).toEqual(true != false);

        expect(Operander.OPERATORS['!='](true, 1)).toEqual(true != 1);
        expect(Operander.OPERATORS['!='](1, '1')).toEqual(1 != '1');
    });
    it("!==", () => {
        expect(Operander.OPERATORS['!=='](true, true)).toEqual(true !== true);
        expect(Operander.OPERATORS['!=='](true, false)).toEqual(true !== false);

        expect(Operander.OPERATORS['!=='](true, 1)).toEqual(true !== 1);
        expect(Operander.OPERATORS['!=='](1, '1')).toEqual(1 !== '1');
    });

    it("in", () => {
        var arr = [0, 'yay', 1];

        expect(Operander.OPERATORS['in']('yay', arr)).toEqual(arr.includes('yay'));
        expect(Operander.OPERATORS['in']('bad', arr)).toEqual(arr.includes('bad'));
    });
    it("!in", () => {
        var arr = [0, 'yay', 1];

        expect(Operander.OPERATORS['!in']('yay', arr)).toEqual(!arr.includes('yay'));
        expect(Operander.OPERATORS['!in']('bad', arr)).toEqual(!arr.includes('bad'));
    });

    it("has", () => {
        var arr = [0, 'yay', 1];

        expect(Operander.OPERATORS['has'](arr, 'yay')).toEqual(arr.includes('yay'));
        expect(Operander.OPERATORS['has'](arr, 'bad')).toEqual(arr.includes('bad'));
    });
    it("!has", () => {
        var arr = [0, 'yay', 1];

        expect(Operander.OPERATORS['!has'](arr, 'yay')).toEqual(!arr.includes('yay'));
        expect(Operander.OPERATORS['!has'](arr, 'bad')).toEqual(!arr.includes('bad'));
    });
});

describe("Variable Extraction", () => {
    it("Should extract variables from a simple rule", () => {
        var simpleVarsRule = ['$.v1', '===', true],
            obj = Operander.ExtractVariables(simpleVarsRule),
            vars = Object.keys(obj);

        expect(vars.length).toEqual(1);
        expect(vars[0]).toEqual('v1');
        expect(obj.v1).toEqual(null);
    });
    it("Should extract variables from a simple rule and resolve variable", () => {
        var simpleVarsRule = ['$.v1', '===', true],
            input = {v1: 'value1'},
            obj = Operander.ExtractVariables(simpleVarsRule, input),
            vars = Object.keys(obj);

        expect(vars.length).toEqual(1);
        expect(vars[0]).toEqual('v1');
        expect(obj.v1).toEqual(input.v1);
    });
    
    it("Should extract variables from a completed rule", () => {
        var complexVarsRule = [['$.v1', '===', true], 'AND', ['$.v2', '===', true]],
            obj = Operander.ExtractVariables(complexVarsRule),
            vars = Object.keys(obj);

        expect(vars.length).toEqual(2);
        expect(vars[0]).toEqual('v1');
        expect(obj.v1).toEqual(null);
        expect(vars[1]).toEqual('v2');
        expect(obj.v2).toEqual(null);
    });
    it("Should extract variables from a completed rule and resolve variables", () => {
        var complexVarsRule = [['$.v1', '===', true], 'AND', ['$.v2', '===', true]],
            input = {v1: 'value1', v2: 'value2'},
            obj = Operander.ExtractVariables(complexVarsRule, input),
            vars = Object.keys(obj);

        expect(vars.length).toEqual(2);
        expect(vars[0]).toEqual('v1');
        expect(obj.v1).toEqual(input.v1);
        expect(vars[1]).toEqual('v2');
        expect(obj.v2).toEqual(input.v2);
    });
    
    it("Should extract variables from a tree'd rule", () => {
        var treeVarsRule = [[['$.v1', '===', true], 'OR', ['$.v2', '===', true]], 'AND', ['$.v3', '===', true]],
            obj = Operander.ExtractVariables(treeVarsRule),
            vars = Object.keys(obj);

        expect(vars.length).toEqual(3);
        expect(vars[0]).toEqual('v1');
        expect(obj.v1).toEqual(null);
        expect(vars[1]).toEqual('v2');
        expect(obj.v2).toEqual(null);
        expect(vars[2]).toEqual('v3');
        expect(obj.v3).toEqual(null);
    });
    it("Should extract variables from a tree'd rule and resolve variables", () => {
        var treeVarsRule = [[['$.v1', '===', true], 'OR', ['$.v2', '===', true]], 'AND', ['$.v3', '===', true]],
            input = {v1: 'value1', v2: 'value2', v3: 'value3'},
            obj = Operander.ExtractVariables(treeVarsRule, input),
            vars = Object.keys(obj);

        expect(vars.length).toEqual(3);
        expect(vars[0]).toEqual('v1');
        expect(obj.v1).toEqual(input.v1);
        expect(vars[1]).toEqual('v2');
        expect(obj.v2).toEqual(input.v2);
        expect(vars[2]).toEqual('v3');
        expect(obj.v3).toEqual(input.v3);
    });
});

describe("SafeGet", () => {
    it("Should get high level props", () => {
        var obj = { test: 'value' };

        expect(Operander.SafeGet(obj, 'test')).toEqual(obj.test);
        expect(Operander.SafeGet(obj, 'bad')).toEqual(obj.bad);
    });

    it("Should get deep props", () => {
        var obj = { test: { stuff: 'value' } };

        expect(Operander.SafeGet(obj, 'test.stuff')).toEqual(obj.test.stuff);
        expect(Operander.SafeGet(obj, 'test.bad')).toEqual(obj.test.bad);
        expect(Operander.SafeGet(obj, 'bad.bad')).toEqual(undefined);
    });
});

describe("ResolveVariable", () => {
    it("Should return non-prefixed keys as values", () => {
        var obj = { not: 'used' };
        expect(Operander.ResolveVariable('test', obj)).toEqual('test');
        expect(Operander.ResolveVariable('$test', obj)).toEqual('$test');
        expect(Operander.ResolveVariable(0, obj)).toEqual(0);
        expect(Operander.ResolveVariable(1, obj)).toEqual(1);
        expect(Operander.ResolveVariable(true, obj)).toEqual(true);
        expect(Operander.ResolveVariable(false, obj)).toEqual(false);
        expect(Operander.ResolveVariable(['test'], obj)).toEqual(['test']);
        expect(Operander.ResolveVariable(['$test'], obj)).toEqual(['$test']);
        expect(Operander.ResolveVariable([0], obj)).toEqual([0]);
        expect(Operander.ResolveVariable([1], obj)).toEqual([1]);
        expect(Operander.ResolveVariable([true], obj)).toEqual([true]);
        expect(Operander.ResolveVariable([false], obj)).toEqual([false]);
    });

    it("Should call provided resolver for resolving values", () => {
        var spy = jasmine.createSpy().and.callFake(() => { return 'value' }),
            val = Operander.ResolveVariable('$.test', spy)

        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test');
        expect(val).toEqual('value');
    });

    it("Should resolve values from object", () => {
        var obj = {
            test: 'value1',
            deep: { stuff: 'value2' }
        },
        result1 = Operander.ResolveVariable('$.test', obj),
        result2 = Operander.ResolveVariable('$.deep.stuff', obj);

        expect(result1).toEqual(obj.test);
        expect(result2).toEqual(obj.deep.stuff);
    });
});

describe("Eval", () => {
    it("Should call appropriate operand", () => {
        Operander.OPERATORS['test'] = jasmine.createSpy().and.callFake(() => { return 'value' });

        var result = Operander.Eval(['parm1', 'test', 'parm2'], null);

        expect(Operander.OPERATORS.test).toHaveBeenCalledTimes(1);
        expect(Operander.OPERATORS.test).toHaveBeenCalledWith('parm1', 'parm2');

        delete Operander.OPERATORS.test;
    });

    it("Should call appropriate operand with resolved variables", () => {
        Operander.OPERATORS['test'] = jasmine.createSpy().and.callFake(() => { return 'value' });

        var obj = {
            test: 'parm1',
            deep: {
                stuff: 'parm2'
            }
        },
            result = Operander.Eval(['$.test', 'test', '$.deep.stuff'], obj);

        expect(Operander.OPERATORS.test).toHaveBeenCalledTimes(1);
        expect(Operander.OPERATORS.test).toHaveBeenCalledWith('parm1', 'parm2');

        delete Operander.OPERATORS.test;
    });
});

describe("Process raw tests", () => {
    var trueRule = [true, '===', true],
        falseRule = [true, '===', false];

    it("Should process simple rules", () => {
        expect(Operander.Process(trueRule)).toEqual(true);
        expect(Operander.Process(falseRule)).toEqual(false);
    });

    it("Should process rules with AND", () => {
        expect(Operander.Process([trueRule, 'AND', trueRule])).toEqual(true);
        expect(Operander.Process([trueRule, 'AND', falseRule])).toEqual(false);
        expect(Operander.Process([falseRule, 'AND', falseRule])).toEqual(false);
        expect(Operander.Process([falseRule, 'AND', trueRule])).toEqual(false);
    });

    it("Should process rules with OR", () => {
        expect(Operander.Process([trueRule, 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([trueRule, 'OR', falseRule])).toEqual(true);
        expect(Operander.Process([falseRule, 'OR', falseRule])).toEqual(false);
        expect(Operander.Process([falseRule, 'OR', trueRule])).toEqual(true);
    });

    it("Should process tree'd rules -- AND/AND", () => {
        expect(Operander.Process([[trueRule, 'AND', trueRule], 'AND', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'AND', trueRule], 'AND', falseRule])).toEqual(false);
        expect(Operander.Process([[trueRule, 'AND', falseRule], 'AND', trueRule])).toEqual(false);
        expect(Operander.Process([[falseRule, 'AND', trueRule], 'AND', trueRule])).toEqual(false);
        expect(Operander.Process([[trueRule, 'AND', falseRule], 'AND', falseRule])).toEqual(false);
        expect(Operander.Process([[falseRule, 'AND', falseRule], 'AND', trueRule])).toEqual(false);
        expect(Operander.Process([[falseRule, 'AND', trueRule], 'AND', falseRule])).toEqual(false);
    });

    it("Should process tree'd rules -- AND/OR", () => {
        expect(Operander.Process([[trueRule, 'AND', trueRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'AND', trueRule], 'OR', falseRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'AND', falseRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[falseRule, 'AND', trueRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'AND', falseRule], 'OR', falseRule])).toEqual(false);
        expect(Operander.Process([[falseRule, 'AND', falseRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[falseRule, 'AND', trueRule], 'OR', falseRule])).toEqual(false);
    });

    it("Should process tree'd rules -- OR/AND", () => {
        expect(Operander.Process([[trueRule, 'OR', trueRule], 'AND', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'OR', trueRule], 'AND', falseRule])).toEqual(false);
        expect(Operander.Process([[trueRule, 'OR', falseRule], 'AND', trueRule])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', trueRule], 'AND', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'OR', falseRule], 'AND', falseRule])).toEqual(false);
        expect(Operander.Process([[falseRule, 'OR', falseRule], 'AND', trueRule])).toEqual(false);
        expect(Operander.Process([[falseRule, 'OR', trueRule], 'AND', falseRule])).toEqual(false);
    });

    it("Should process tree'd rules -- OR/OR", () => {
        expect(Operander.Process([[trueRule, 'OR', trueRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'OR', trueRule], 'OR', falseRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'OR', falseRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', trueRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[trueRule, 'OR', falseRule], 'OR', falseRule])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', falseRule], 'OR', trueRule])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', trueRule], 'OR', falseRule])).toEqual(true);
    });

    it("Should process deep tree'd rules", () => {
        expect(Operander.Process([[trueRule, 'OR', [trueRule, 'AND', trueRule]], 'OR', [trueRule, 'OR', [trueRule, 'AND', trueRule]]])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', [trueRule, 'AND', trueRule]], 'OR', [trueRule, 'OR', [trueRule, 'AND', trueRule]]])).toEqual(true);
        expect(Operander.Process([[trueRule, 'OR', [trueRule, 'AND', trueRule]], 'OR', [falseRule, 'OR', [trueRule, 'AND', trueRule]]])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', [trueRule, 'AND', trueRule]], 'OR', [falseRule, 'OR', [trueRule, 'AND', trueRule]]])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', [falseRule, 'AND', trueRule]], 'OR', [falseRule, 'OR', [trueRule, 'AND', trueRule]]])).toEqual(true);
        expect(Operander.Process([[falseRule, 'OR', [falseRule, 'AND', trueRule]], 'OR', [falseRule, 'OR', [falseRule, 'AND', trueRule]]])).toEqual(false);
    });
});

describe("Process", () => {
    var obj = {
        test: 'value1',
        deep: {
            stuff: 'value2'
        }
    };

    it("Should resolve", () => {
        expect(Operander.Process(['$.test', '===', 'value1'], obj)).toEqual(true);
        expect(Operander.Process(['$.test', '===', 'bad'], obj)).toEqual(false);
        expect(Operander.Process(['$.test', '!==', 'bad'], obj)).toEqual(true);
        expect(Operander.Process(['$.test', '!==', 'value1'], obj)).toEqual(false);

        expect(Operander.Process(['$.deep.stuff', '===', 'value2'], obj)).toEqual(true);
        expect(Operander.Process(['$.deep.stuff', '===', 'bad'], obj)).toEqual(false);
        expect(Operander.Process(['$.deep.stuff', '!==', 'bad'], obj)).toEqual(true);
        expect(Operander.Process(['$.deep.stuff', '!==', 'value2'], obj)).toEqual(false);

        expect(Operander.Process(['$.bad.bad', '===', 'value2'], obj)).toEqual(false);
        expect(Operander.Process(['$.bad.bad', '===', 'bad'], obj)).toEqual(false);
        expect(Operander.Process(['$.bad.bad', '!==', 'bad'], obj)).toEqual(true);
        expect(Operander.Process(['$.bad.bad', '!==', 'value2'], obj)).toEqual(true);
    });
});