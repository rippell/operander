# Operander

JSON Serializable operands with the ability to resolve against a given object or callback method.
Of course without the use of `eval`!

Designed to read and flow as a natural if-statement and allow extendability where needed.

Demo and RuleSet builder coming soon, as well as more operators!

```JavaScript
// Template
// ["value or $.ref", "operator", "value or $.ref"]

let ExampleRule = [true, "===", true]


let obj = {
  "example": "value",
  "deep": {
    "value": "neat!"
  }
}
let simpleRule1 = ["$.example", "===", "value"];
let simpleRule2 = ["$.deep.value", "===", "neat!"];

Operander.Process(simpleRule1, obj); // true
Operander.Process(simpleRule2, obj); // true

let complexRule = [["$.example", "===", "value"], "AND", ["$.deep.value", "===", "neat!"]];
Operander.Process(complexRule, obj); // true

let moarComplexRule = [
  ["$.example", "===", "value"], 
  "AND", 
  [
    ["$.deep.value", "===", "neat!"], 
    "OR", 
    [true, '===', 'false']
  ]
];
Operander.Process(moarComplexRule, obj); // true

// A resolver function can be given instead of of an object
let resolver = key => (key === 'yay') ? 'Example!' : 'ehhh...';
Operander.Process(["$.yay", "===", "Example!"], resolver);  // True
Operander.Process(["$.test", "===", "Example!"], resolver); // False
```


## Install

```sh
npm i @rippell/operander --save

yarn add @rippell/operander
```


## Basic Use

This concept is basically made up of:
* [Statements](#statements) ( `[ Left Operand, operator, Right Operand ]` )
* [Statement Chaining](#chaining) ( `"AND"` and `"OR"` )

```
       Operator            Chaining
          |               /
         \'/             /
[ [ X,  "===",   Y ], "AND", [ I, "!==" Z ] ]
    ^            ^
    |            |
    `- Operands -`
```

### Statements

The 2nd entry in a rule is always the 'operator', telling which function to call to utilize the two values on either side of it.
See [available operators](#available-operators) below.

Operands can either be solid values or references.
Solid values (true, 123, 'cool strings', etc..) are treated as the value to test against.

References are strings and identified by a `$.` prefix.
If providing an object to `Operander.Process`, this reference can be a period-delimited string into the object.
A safe getting (`Operander.SafeGet`) function is used and returns `undefined` if an intermediate object doesn't exist as to avoid "null pointers".

If providing a resolver function instead of an object, the `$.` prefix is used to identify to call the resolver.
Resolver will receive the key without the `$.` prefix, which allows you to use it however you please, such as complex selectors.

> Note that the resolver must be synchronous.
> If looking to collect data from a remote resource, see [Extracting Variables](#extracting-variables).


#### Available Operators

| Operator   | Description                            | Example(s) (true results) |
| :--------: | :------------------------------------- | :--------------------- |
| `<`        | Less Than                              | `[1, "<", 5]` |
| `<=`       | Less Than or Equal To                  | `[ [1, "<=", 5], "AND" [5, "<=", 5] ]`  |
| `>`        | Greater Than                           | `[5, ">", 1]` |
| `>=`       | Greater Than or Equal To               | `[ [5, ">=", 1], "AND", [5, ">=", 5] ]` |
| `between`  | Number Between Two Numbers             | `[5, "between", [1, 10] ]` |
| `in-range` | Number Between Two Numbers (inclusive) | `[ [5, "in-range", [1, 10] ], "AND", [1, "in-range", [1, 10] ], "AND", [10, "in-range", [1, 10]] ]` |
| `==`       | Equals                                 | `[ [true, "==", true], "AND", [true, "==", 1], "AND", [1, "==", "1"] ]` |
| `===`      | Equals Strict                          | `[ [1, "===", 1], "AND", ["1", "===", "1"] ]` |
| `!=`       | Not Equals                             | `[true, "!=", false]` |
| `!==`      | Not Equals Strict                      | `[ [true, "!==", false], "AND", [true, "!==", 1], "AND", [1, "!==", "1"] ]` |
| `in`       | Value in Array                         | `[ [2, "in", [1,2,3]], "AND", ["test", "in", ["test", "stuff"] ] ]` |
| `!in`      | Value not in Array                     | `[ [5, "!in", [1,2,3]], "AND", ["bad", "!in", ["test", "stuff"] ] ]` |
| `has`      | Array has Value                        | `[ [1,2,3], "has", 2]], "AND", [["test", "stuff"], "has", "test" ] ]` |
| `!has`     | Array doesn't have Value               | `[ [[1,2,3], "!has", 5], "AND", [["test", "stuff"], "!has", "bad" ] ]` |

Custom Operators can be added by adding to the `Operander.OPERATORS` object.
```JavaScript
Operander.OPERATORS['example'] = (v1, v2) => Math.abs(v1) === v2;
```

### Chaining

As any if-statement would have, [statements](#Statements) can be chained and grouped together with `AND` and `OR`, as well as array nesting.
Imagine the `[` and `]` are `(` and `)` in an if statement and you really like encapsulating each assertion.

```JavaScript
// If statement in comparison to a Operander statement
if ( true   ===   true    and    false   ===   false )
   [[true, "===", true], "AND", [false, "===", false]]

if ( ( true   ===   true    or    true   ===   false )   and   ( false   ===   false    or    false   ===   true ))
   [ [[true, "===", true], "OR", [true, "===", false]], "AND", [[false, "===", false], "OR", [false, "===", true]]]
```


## Extracting Variables

In some cases, the data you want to assert against isn't nicely organized into a single object (yet).
Some of the data may be on a remote resource and require asynchronous functions to gather it.

`Operander.ExtractVariables` allows you to retrieve all variables that would need to be resolved.
It returns an object with keys being each `$.` prefixed rule value found.
If given an object, the values will be populated as found.

By gathering what variables you need, requests can be shipped off to gather the data into an object for use with Operander.
Especially handy if looking to dynamically form GraphQL queries/mutations.

> Note the keys returned are in period-delimited format.
> Open for feedback if an exploded object makes more sense.
